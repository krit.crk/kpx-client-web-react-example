import React, { useEffect, useState } from 'react';
import { useImmer } from 'use-immer'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import KpxClient from 'kpx-client-web'
import { Button, FormGroup, Label, Input } from 'reactstrap'
import { Box, Row } from './Layout'
import { Todo } from './Todo'
function App() {

  useEffect(() => {
    KpxClient.init({ app_type: "5f45e1e76a7de33d74d33912" })
    authCheck()
  }, [])

  var [state, setState] = useImmer({
    isAuth: false,
  });

  async function openAuthPage() {
    await KpxClient.openAuthPage();
    authCheck()
  }

  async function signOut() {
    await KpxClient.signOut();
    authCheck()
  }

  async function authCheck() {
    var auth = await KpxClient.isAuth()
    setState(d => { d.isAuth = auth })
    return auth
  }


  if (state.isAuth)
    return <div className="App">
      <Row alignItems="center">
        <h1>{KpxClient.user.user_profile.fullname}</h1>
        <Box width={50} />
        <Button color="danger" onClick={signOut}>signOut</Button>
      </Row>
      <Box height={20} />
      <Todo />
    </div>
  else
    return (
      <div className="App">
        <Button color="danger" onClick={openAuthPage}>Auth</Button>
      </div>
    );
}



export default App;
