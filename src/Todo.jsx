import React, { useEffect, useState } from 'react';
import { useImmer } from 'use-immer'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import KpxClient from 'kpx-client-web'
import { Button, FormGroup, Label, Input } from 'reactstrap'
import { Box, Row } from './Layout'
export function Todo() {

    useEffect(() => {
        getTodo();
    }, [])

    var [state, setState] = useImmer({
        newTodo: "",
        listTodo: [],
        editId: "",
        editTodo: "",
        editCheck: false
    });

    async function addTodo() {
        var res = await KpxClient.docInsert({ todo_text: state.newTodo, todo_check: false });
        console.log(res);
        setState(d => { d.newTodo = "" });
        await getTodo();
    }

    async function getTodo() {
        var res = await KpxClient.docQueryString();
        console.log(res);
        setState(d => { d.listTodo = res.data });
    }

    async function toggleTodo(id, check) {
        var update = [
            { field: "todo_check", action: "set", value: check }
        ]
        var res = await KpxClient.docUpdate(id, update)
        console.log(res);
        await getTodo()
    }

    async function delTodo(id) {
        var res = await KpxClient.docDelete(id)
        console.log(res);
        await getTodo()
    }

    async function replaceTodo() {
        var replace = {
            _id: state.editId,
            todo_text: state.editTodo,
            todo_check: state.editCheck
        }
        var res = await KpxClient.docReplace(state.editId, replace)
        console.log(res);
        setState(d => {
            d.editId = null
            d.editTodo = null
            d.editCheck = null
        })
        await getTodo()
    }

    async function editTodo(todo) {
        //var todo = state.listTodo.find(i=>i._id == id)
        setState(d => {
            d.editId = todo._id
            d.editTodo = todo.todo_text
            d.editCheck = todo.todo_check
        })
    }

    return (
        <>
            <Row>
                <Box width={200}><Input value={state.newTodo} onChange={({ target: { value } }) => setState(d => { d.newTodo = value })} /> </Box>
                <Button color="primary" onClick={addTodo}>Add</Button>
            </Row>
            <Box height={20} />
            {state.listTodo.map(t => (
                <Box key={t._id} width="250">
                    <Row alignItems="center">
                        <input type="checkbox" checked={t.todo_check} onChange={_ => toggleTodo(t._id, !t.todo_check)} />
                        <Box width={20} />
                        {t.todo_text}
                        <Box width={20} />
                        <Box floatRight>
                            <Row>
                                <Button color="secondary" outline size="sm" onClick={_ => editTodo(t)}>Edit</Button>
                                <Box width={10} />
                                <Button color="danger" outline size="sm" onClick={_ => delTodo(t._id)}>Delete</Button>
                            </Row>
                        </Box>

                    </Row>
                    <Box height={10} />
                </Box>
            ))}
            {state.editId &&
                <Row>
                    <Box width={200}><Input value={state.editTodo} onChange={({ target: { value } }) => setState(d => { d.editTodo = value })} /> </Box>
                    <Button color="primary" onClick={replaceTodo}>Save</Button>
                </Row>
            }

        </>
    )
}