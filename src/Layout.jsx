import React from 'react';
export function Box({ width = 0, height = 0, floatRight = false, children }) {
    return <div style={{
        width: width ? `${width}px` : "auto",
        height: height ? `${height}px` : "auto",
        marginLeft: floatRight ? "auto" : "0px"
    }}>{children}</div>
}
export function Row({ children, alignItems = "start" }) {
    return <div style={{
        width: "100%",
        display: "flex",
        flexDirection: "row",
        alignItems
    }}>{children}</div>
}
// export function Right({ children}){
//     return <div style=>{children}</div>
// }